

<h1>Lista de Tarefas com Laravel</h1>

@if(count($lista) > 0)
    <ul>
        @foreach($lista as $item)
        <li>
            {{ $item->item }} <a href="delete/{{ $item->id }}">X</a> 
                /
             <a href="alterar/{{ $item->id }}">Alterar</a>
        </li>
        @endforeach
    </ul>
@else
    <h2>Não há itens</h2>
@endif

<hr>

<form method="POST">

    {{ csrf_field() }}

    Item:
    <input type="text" name="item">
    <input type="submit" value="+">

</form>

<hr>

  