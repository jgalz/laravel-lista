<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Lista;

class HomeController extends Controller {

    public function home() {
        //Pegando dados do banco
        $lista = Lista::all();
        //Armazenando dados do banco 
        $array = array('lista' => $lista);
        //Retornando na view
        return view('home', $array);
    }

    public function add(Request $req) {
        if($req->has('item')) {
            $item = $req->input('item');
            $lista = new Lista;
            $lista->item = $item;
            $lista->save();
        } 
        return redirect('/'); 
    }

    public function del($id) {
        Lista::find($id)->delete();

        return redirect('/');
    }

    public function getItem($id) {
        $lista = Lista::where('id', '=', $id)->get()->first();

        return view('alterar', compact('lista'));
    }

    public function alt($id, Request $req) {

        if($req->has('item')) {
            $item = $req->input('item');
            $lista = Lista::find($id);
            $lista->item = $item;
            $lista->save();
        }
        return redirect('/');

    }





}
